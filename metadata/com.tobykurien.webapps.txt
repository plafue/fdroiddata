Categories:Internet
License:MIT
Web Site:https://github.com/tobykurien/WebApps/blob/HEAD/README.md
Source Code:https://github.com/tobykurien/webapps
Issue Tracker:https://github.com/tobykurien/webapps/issues

Auto Name:WebApps
Summary:Sandbox for webapps
Description:
Provide a secure way to browse popular webapps by eliminating referrers, 3rd
party requests, cookies, cross-site scripting, etc.

It accomplishes this by providing a sandbox for multiple webapps (like Google's
apps, Facebook, Twitter, etc.). Each webapp will run in it's own sandbox, with
3rd party requests (images, scripts, iframes, etc.) blocked, and all external
links opening in an external default web browser (which should have cookies,
plugins, flash, etc. disabled).

By default, all HTTP requests are blocked (only HTTPS allowed). This improves
security, especially on untrusted networks. The app can also handle HTTPS links
and open them in their own sandbox.

Based on [[com.tobykurien.google_news]].
.

Repo Type:git
Repo:https://github.com/tobykurien/webapps

Build:v1-beta2,2
    disable=gradle issue
    commit=0dbf7c27c0c1a3628a97b69a3f7139397b640582
    gradle=yes
    rm=libs/*.jar

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:v1-beta2
Current Version Code:2

