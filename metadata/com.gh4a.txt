AntiFeatures:UpstreamNonFree,NonFreeNet
Categories:Development
License:Apache2
Web Site:
Source Code:https://github.com/slapperwan/gh4a
Issue Tracker:https://github.com/slapperwan/gh4a/issues

Auto Name:OctoDroid
Summary:GitHub client
Description:
Supports all the basic Github.com features, such as:

* Watch/unwatch repositories
* View branches/tags, pull requests etc. in each repo
* View and comment on issues
* Home page feed of recent actions and news
* Search for repositories
* Diff viewer with colorized HTML
* Trending repos (today, week, month, forever)

Uses OAuth2: your password is used to generate a key and is then
destroyed. The key can be revoked any time by visiting github.com.
Formerly known as GH4A. This application is currently in BETA: please
report any issues to the issue tracker.

Newer versions exist elsewhere, but now include proprietary libraries,
so updates are unlikely here.

[https://github.com/slapperwan/gh4a/blob/master/CHANGES Changelog]
.

Repo Type:git
Repo:https://github.com/slapperwan/gh4a.git

Build:3.6,36
    commit=53d2e5bbfd76d8ad4a779913df7b3f193fb19a3e
    patch=bugsense.patch
    gradle=yes
    prebuild=rm -rf libs/android-support-v4.jar

Build:3.7.1,38
    commit=v3.7.1
    patch=mint.patch,gradle.patch
    gradle=yes
    srclibs=GH4AGitHubAPI@3d1878bd48d9aa3817a91b5a193b600c31e0406,ViewPagerIndicator@2.4.1
    prebuild=rm -rf libs/*.jar && \
      cp -a $$GH4AGitHubAPI$$/src/org src && \
      sed -i -e '/populov/d' -e '/mavenCentral/imavenLocal()' build.gradle && \
      pushd $$ViewPagerIndicator$$ && \
      sed -i 's;<android-maven.version>3.3.0</android-maven.version>;<android-maven.version>3.7.0</android-maven.version>;' ../pom.xml pom.xml && \
      sed -i -e 's/apklib/aar/g' pom.xml && \
      rm libs/* && \
      cd .. && $$MVN3$$ install && popd

Auto Update Mode:None
Update Check Mode:Tags
Current Version:3.7.1
Current Version Code:38

